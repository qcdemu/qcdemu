<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>QCDemu</name>
    <message>
        <location filename="qcdemu.cpp" line="226"/>
        <source>Select image to mount</source>
        <translation>Wybierz obraz do zamontowania</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="38"/>
        <location filename="qcdemu.cpp" line="43"/>
        <location filename="qcdemu.cpp" line="77"/>
        <location filename="qcdemu.cpp" line="97"/>
        <location filename="qcdemu.cpp" line="191"/>
        <location filename="qcdemu.cpp" line="250"/>
        <location filename="qcdemu.cpp" line="272"/>
        <location filename="qcdemu.cpp" line="283"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="338"/>
        <location filename="qcdemu.cpp" line="421"/>
        <source>CDemu not running</source>
        <translation>Nie włączono CDemu</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="38"/>
        <location filename="qcdemu.cpp" line="77"/>
        <source>No CDemu devices detected. Maybe you forgot to start the CDemu daemon?</source>
        <translation>Nie wykryto urządzeń CDemu. Może zapomniałeś włączyć daemona CDemu?</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="227"/>
        <source>All images (*.iso *.img *.wav *.cue *.toc *.b5t *.b6t *.cdi *.nrg *.mds *.xmd *.ccd *.c2d *.cif *.daa);;ISO images (*.iso *.img *.wav);;CUE images (*.cue);;TOC files (*.toc);;BlindWrite 5/6 images (*.b5t *.b6t);;DiscJuggler images (*.cdi);;Nero Burning Rom images (*.nrg);;Alcohol 120% images (*.mds *.xmd);;CloneCD images (*.ccd);;Easy Media Creator images (*.c2d);;Easy CD Creator &lt;= v5.0 images (*.cif);;PowerISO direct access archives (*.daa)</source>
        <translation>Wszystkie obrazy (*.iso *.img *.wav *.cue *.toc *.b5t *.b6t *.cdi *.nrg *.mds *.ccd);;Obrazy ISO (*.iso *.img *.wav);;Obrazy CUE (*.cue);;Pliki TOC (*.toc);;Obrazy BlindWrite 5/6 (*.b5t *.b6t);;Obrazy DiscJuggler (*.cdi);;Obrazy Nero Burning Rom (*.nrg);;Obrazy Alcohol 120% (*.mds *.xmd);;Obrazy CloneCD (*.ccd);;Obrazy Easy Media Creator (*.c2d);;Obrazy Easy CD Creator &lt;= v5.0 (*.cif);;Archiwa PowerISO (*.daa)</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="346"/>
        <source>Device %1: </source>
        <translation>Urządzenie %1: </translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="358"/>
        <source>Not mounted</source>
        <translation>Niezamontowane</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="411"/>
        <source>Exit</source>
        <translation>Wyjdź</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="420"/>
        <source>%1 available device(s)</source>
        <translation>%1 dostępnych urządzeń</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="370"/>
        <source>Use system bus</source>
        <translation>Użyj szyny systemowej</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="43"/>
        <source>Your CDemu daemon is too old. Please upgrade to v1.2.0+.</source>
        <translation>Twój daemon CDemu jest zbyt stary. Zaktualizuj go do wersji 1.2.0 ub nowszej.</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="97"/>
        <source>Couldn&apos;t access DBus. Please check your system configuration.</source>
        <translation>Nie udało się połączyć z DBus. Proszę sprawdzić konfigurację systemu.</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="160"/>
        <location filename="qcdemu.cpp" line="166"/>
        <source>QCDemu</source>
        <translation>QCDemu</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="152"/>
        <source>CDemu has been started</source>
        <translation>CDemu zostało włączone</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="145"/>
        <source>CDemu was stopped</source>
        <translation>CDemu zostało wyłączone</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="174"/>
        <source>Device %1 has been mounted.</source>
        <translation>Urządzenie %1 zostało zamontowane.</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="175"/>
        <source>Device %1 has been unmounted.</source>
        <translation>Urządzenie %1 zostało odmontowane.</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="191"/>
        <source>No free CDemu devices detected to mount on.</source>
        <translation>Brak wolnych urządzeń CDemu.</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="250"/>
        <source>Couldn&apos;t open %1</source>
        <translation>Nie udało się otworzyć %1</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="376"/>
        <source>Show notifications</source>
        <translation>Pokazuj powiadomienia</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="405"/>
        <source>About...</source>
        <translation>O QCDemu...</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="459"/>
        <source>About QCDemu</source>
        <translation>O QCDemu</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="459"/>
        <source>CDemu v</source>
        <translation>CDemu v</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="386"/>
        <source>Recent files</source>
        <translation>Ostatnio używane</translation>
    </message>
    <message>
        <location filename="qcdemu.cpp" line="387"/>
        <source>Purge list</source>
        <translation>Wyczyść listę</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>QCDemu</source>
        <translation>QCDemu</translation>
    </message>
    <message>
        <location filename="main.cpp" line="54"/>
        <source>No systray detected.</source>
        <translation>Nie wykryto zasobnika systemowego.</translation>
    </message>
</context>
</TS>
