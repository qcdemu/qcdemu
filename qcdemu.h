/*
    QCDemu, a qt4-based frontend for cdemu 1.2+
    Copyright (C) 2008-2010 Michał "mziab" Ziąbkowski

    QCDemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QCDemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QCDemu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef H_QCDEMU
#define H_QCDEMU

#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include <QtDBus/QtDBus>

class QDBusMessage;
class QDBusConnection;
class QDBusInterface;
class QAction;
class QMenu;
class QSystemTrayIcon;
class QMessageBox;
class QSettings;

class QCDemu : public QWidget
{
Q_OBJECT

public:
	QCDemu(QWidget * parent = 0);
	QCDemu(const QStringList &filenames);
	~QCDemu();

private slots:
	void clicked();
	void promptMount(const int device);
	void promptMount(const QSystemTrayIcon::ActivationReason &reason);
	void unmount(const int device);
	void updateActions();
	void selectBus(const bool systemBus);
	bool guessBus();
	void deviceChange(const int device);
	void daemonStopped();
	void daemonStarted();
	void setNotify(const bool setting) { showNotifications=setting;};
	void showAbout();
	void recentClicked();
	void recentClear();

private:
	QVariantList getStatus(const int device);
	unsigned int getDeviceNumber();

	bool isMounted(const int device);
	void showInfo(const QString &message);
	void showError(const QString &message);
	bool mount(const QStringList &filenames);
	bool mount(const int device, const QStringList &filenames);
	void loadSettings();
	void loadRecent();
	void dbusConnect(const bool busType);
	QStringList getFileNames();
	bool daemonIsNewEnough();

	unsigned int mountedNumber;
	unsigned int device_number;
	QMenu *menu;
	QSystemTrayIcon *tray;
	QAction *inactive;
	bool useSystemBus;
	bool showNotifications;
	QString lastDir;
	QDBusInterface *iface;
	QAction *config;
	QAction *notify;
	QAction *about;
	QAction *quit;
	QStringList recentFiles;
	QMenu *recent;
};

#endif
