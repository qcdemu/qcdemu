/*
    QCDemu, a Qt4-based frontend for CDemu 1.2+
    Copyright (C) 2008-2010 Michał "mziab" Ziąbkowski

    QCDemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QCDemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QCDemu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qcdemu.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	// i18n stuff
	#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
	QTextCodec::setCodecForTr(QTextCodec::codecForLocale ());
	#endif
	QTranslator trans;
	QTranslator qt_trans;
	QString lang = QLocale::system().name();
	qt_trans.load( "qt_" + lang,
		QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	trans.load("qcdemu_" + lang, TRANSLATION_PATH);
	app.installTranslator(&trans);
	app.installTranslator(&qt_trans);

	// handle command-line and bail out
	QStringList params = app.arguments();
	if (params.count() > 1)
	{
		QStringList files;
		QDir directory;

		for (int i = 1; i < params.count(); i++)
			files.append(directory.absoluteFilePath(params[i]));

		QCDemu mounter(files);
		return 0;
	}

	// no command-line arguments? let's draw the gui
	if (!QSystemTrayIcon::isSystemTrayAvailable())
	{
		QMessageBox::critical(0, QObject::tr("QCDemu"),
		QObject::tr("No systray detected."));
		return 1;
	}

	QCDemu mounter;
	app.setQuitOnLastWindowClosed(false);

	return app.exec();
}

