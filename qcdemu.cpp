/*
    QCDemu, a Qt4-based frontend for CDemu 1.2+
    Copyright (C) 2008-2010 Michał "mziab" Ziąbkowski

    QCDemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QCDemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QCDemu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qcdemu.h"

// default constructor
QCDemu::QCDemu(QWidget * parent):QWidget(parent)
{
	loadSettings();
	dbusConnect(useSystemBus);

	// create menu and systray icon
	menu = new QMenu(this);
	tray = new QSystemTrayIcon(this);

	loadRecent();

	// check if any devices are detected
	if (getDeviceNumber() == 0)
	{
			// if auto-detection fails, restore the old setting and issue an error
			if (!guessBus())
				QMessageBox::critical(this, tr("Error"), tr("No CDemu devices detected. "
				"Maybe you forgot to start the CDemu daemon?"));
			else
				useSystemBus = !useSystemBus;
	}
	else if (!daemonIsNewEnough())
	{
		QMessageBox::critical(this, tr("Error"), tr("Your CDemu daemon is too old. "
		"Please upgrade to v1.2.0+."));
	}

	// left click action
	connect(tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this,
		SLOT(promptMount(QSystemTrayIcon::ActivationReason)));

	// the finishing touches
	updateActions();
	tray->setContextMenu(menu);
	tray->show();
	iface->connection().connect("net.sf.cdemu.CDEMUD_Daemon", "/CDEMUD_Daemon", "net.sf.cdemu.CDEMUD_Daemon",
			"DeviceStatusChanged", this, SLOT(deviceChange(int)));
	iface->connection().connect("net.sf.cdemu.CDEMUD_Daemon", "/CDEMUD_Daemon", "net.sf.cdemu.CDEMUD_Daemon",
			"DaemonStarted", this, SLOT(daemonStarted()));
	iface->connection().connect("net.sf.cdemu.CDEMUD_Daemon", "/CDEMUD_Daemon", "net.sf.cdemu.CDEMUD_Daemon",
			"DaemonStopped", this, SLOT(daemonStopped()));
}

// leaner constructor for handling command line
QCDemu::QCDemu(const QStringList &filenames)
{
	loadSettings();
	loadRecent();
	dbusConnect(useSystemBus);

	// warn if no devices detected
	device_number = getDeviceNumber();

	if (device_number == 0)
	{
		// if auto-detection fails, restore the old setting and issue an error
		if (!guessBus())
			QMessageBox::critical(this, tr("Error"), tr("No CDemu devices detected. "
			"Maybe you forgot to start the CDemu daemon?"));
		else
			useSystemBus = !useSystemBus;
	}

	// try to mount given images
	mount(filenames);

}

// initialize DBus connection
void QCDemu::dbusConnect(const bool busType)
{
	const QDBusConnection bus = (busType) ? QDBusConnection::systemBus() : QDBusConnection::sessionBus();

	iface = new QDBusInterface( "net.sf.cdemu.CDEMUD_Daemon", "/CDEMUD_Daemon",
				"net.sf.cdemu.CDEMUD_Daemon", bus );

	if(!bus.isConnected())
	{
		QMessageBox::critical(this, tr("Error"), tr("Couldn't access DBus. "
					"Please check your system configuration."));
		qFatal("Couldn't access DBus. Please check your system configuration.");
	}
}

// load settings
void QCDemu::loadSettings()
{
	QSettings settings("mziab", "qcdemu");
	useSystemBus = settings.value("useSystemBus", true).toBool();
	showNotifications = settings.value("showNotifications", true).toBool();
	lastDir = settings.value("lastDir", ".").toString();

	if (!QFile(lastDir).exists())
		lastDir = ".";
}

// load recent images
void QCDemu::loadRecent()
{
	QSettings settings("mziab", "qcdemu");
	recentFiles = settings.value("recentFiles").toStringList();

	// just in case someone tampered with the files
	while (recentFiles.count() > 5)
		recentFiles.removeLast();
}

// flush settings on exit
QCDemu::~QCDemu()
{
	QSettings settings("mziab", "qcdemu");
	settings.setValue("useSystemBus", useSystemBus);
	settings.setValue("showNotifications", showNotifications);
	if (lastDir != ".")
		settings.setValue("lastDir", lastDir);

	settings.setValue("recentFiles", recentFiles);

	// clean up some variables
	delete iface;
}

// run when CDemu is stopped
void QCDemu::daemonStopped()
{
	showInfo(tr("CDemu was stopped"));
	updateActions();
}

// run when CDemu is started
void QCDemu::daemonStarted()
{
	showInfo(tr("CDemu has been started"));
	updateActions();
}


// show information baloon in systray
void QCDemu::showInfo(const QString &message)
{
	tray->showMessage(tr("QCDemu"), message, QSystemTrayIcon::Information, 5000);
}

// show error baloon in systray
void QCDemu::showError(const QString &message)
{
	tray->showMessage(tr("QCDemu"), message, QSystemTrayIcon::Critical, 5000);
}

// run when a device changes status
void QCDemu::deviceChange(const int device)
{
	if (showNotifications)
	{
		QString msg = (isMounted(device)) ? QString(tr("Device %1 has been mounted.")).arg(device) :
			QString(tr("Device %1 has been unmounted.")).arg(device);
		showInfo(msg);
	}

	updateActions();
}

// mounts the image in the first available device
bool QCDemu::mount(const QStringList &filenames)
{
	for (unsigned int i = 0; i < device_number; i++)
	{
		if (!isMounted(i))
			return mount(i, filenames);
	}

	QMessageBox::critical(this, tr("Error"), tr("No free CDemu devices detected to mount on."));
	return(false);
}

// prompts for path and mounts image
void QCDemu::promptMount(const int device)
{
	QStringList filenames = getFileNames();

	if (filenames.isEmpty())
		return;

	mount(device, filenames);
}

// prompts for path and mounts images in the first available device (on left click)
void QCDemu::promptMount(const QSystemTrayIcon::ActivationReason &reason)
{
	if (reason != QSystemTrayIcon::Trigger || device_number == 0)
		return;

	QStringList filenames = getFileNames();

	if (filenames.isEmpty())
		return;

	mount(filenames);
}

// prompt for image name(s)
QStringList QCDemu::getFileNames()
{
	if (!QFile(lastDir).exists())
		lastDir = ".";

	QStringList filenames = QFileDialog::getOpenFileNames(this, tr("Select image to mount"), lastDir,
	tr("All images (*.iso *.img *.wav *.cue *.toc *.b5t *.b6t *.cdi *.nrg *.mds *.xmd *.ccd *.c2d *.cif *.daa);;"
	"ISO images (*.iso *.img *.wav);;"
	"CUE images (*.cue);;"
	"TOC files (*.toc);;"
	"BlindWrite 5/6 images (*.b5t *.b6t);;"
	"DiscJuggler images (*.cdi);;"
	"Nero Burning Rom images (*.nrg);;"
	"Alcohol 120\% images (*.mds *.xmd);;"
	"CloneCD images (*.ccd);;"
	"Easy Media Creator images (*.c2d);;"
	"Easy CD Creator <= v5.0 images (*.cif);;"
	"PowerISO direct access archives (*.daa)"));
	return filenames;
}

// try to mount given images using given device
bool QCDemu::mount(const int device, const QStringList &filenames)
{
	// check whether the files are readable
	foreach(QString file, filenames)
	{
		if (!QFileInfo(file).isReadable())
		{
			QMessageBox::critical(this, tr("Error"), tr("Couldn't open %1").arg(file));
			return false;
		}
	}

	// try to mount
	QDBusMessage reply = iface->call("DeviceLoad", device, filenames, QMap<QString, QVariant>());

	if (reply.type() != QDBusMessage::ErrorMessage)
	{
		loadRecent();

		lastDir = QFileInfo(filenames.first()).absolutePath();
		if (!recentFiles.contains(filenames.first()))
			recentFiles.prepend(filenames.first());

		while (recentFiles.count() > 5)
			recentFiles.removeLast();

		return true;
	}
	else
	{
		QMessageBox::critical(this, tr("Error"), reply.errorMessage());
		return false;
	}
}

// unmounts given device
void QCDemu::unmount(const int device)
{
	QDBusMessage reply = iface->call("DeviceUnload", device);

	if (reply.type() == QDBusMessage::ErrorMessage)
		QMessageBox::critical(this, tr("Error"), reply.errorMessage());
}

// run when an action was clicked
void QCDemu::clicked()
{
	QAction *action = qobject_cast<QAction *>(sender());
	if (action)
	{
		unsigned int device=action->data().toInt();

		if (isMounted(device))
			unmount(device);
		else
			promptMount(device);
	}
}

// run when a recent file entry was clicked
void QCDemu::recentClicked()
{
	QAction *action = qobject_cast<QAction *>(sender());
	if (action)
	{
		QString file = action->data().toString();
		if(!mount(QStringList(file)))
		{
			recentFiles.removeAll(file);
			updateActions();
		}
	}
}

// checks whether a device is mounted or not
bool QCDemu::isMounted(const int device)
{
	QDBusMessage reply = iface->call("DeviceGetStatus", device);
	return(reply.arguments().first().toBool());
}

// query all devices and create actions
void QCDemu::updateActions()
{
	// remove all actions in case it's not the first run
	menu->clear();

	device_number = getDeviceNumber();
	mountedNumber = 0;

	if (device_number == 0)
	{
		// add Inactive action
		inactive = new QAction(tr("CDemu not running"), this);
		menu->addAction(inactive);
	}
	else
	{
		for (unsigned int i = 0; i < device_number; i++)
		{
			QVariantList status = getStatus(i);
			QString text = QString(tr("Device %1: ")).arg(i);

			if (status.first().toBool())
			{
				QString file = status[1].toString();

				// get base filename
				text += QFileInfo(file).fileName();
				mountedNumber++;
			}
			else
			{
				text += tr("Not mounted");
			}

			QAction *a = new QAction(text, this);
			a->setData(i);
			menu->addAction(a);
			connect(a, SIGNAL(triggered()), this, SLOT(clicked()));
		}
	}

	// add config
	menu->addSeparator();
	config = new QAction(tr("Use system bus"), this);
	config->setCheckable(true);
	config->setChecked(useSystemBus);
	connect(config, SIGNAL(toggled(bool)), this, SLOT(selectBus(bool)));
	menu->addAction(config);

	notify = new QAction(tr("Show notifications"), this);
	notify->setCheckable(true);
	notify->setChecked(showNotifications);
	connect(notify, SIGNAL(toggled(bool)), this, SLOT(setNotify(bool)));
	menu->addAction(notify);

	// add recent files
	if (!recentFiles.isEmpty())
	{
		recent = new QMenu(this);
		recent->setTitle(tr("Recent files"));
		QAction *clear = new QAction(tr("Purge list"), this);
		recent->addAction(clear);
		recent->addSeparator();
		connect(clear, SIGNAL(triggered()), this, SLOT(recentClear()));

		for (int i = 0; i < recentFiles.count(); i++)
		{
			QAction *a = new QAction(QFileInfo(recentFiles[i]).fileName(), this);
			a->setData(recentFiles[i]);
			recent->addAction(a);
			connect(a, SIGNAL(triggered()), this, SLOT(recentClicked()));
		}

		menu->addMenu(recent);
	}

	// add About
	menu->addSeparator();
	about = new QAction(tr("About..."), this);
	about->setCheckable(false);
	connect(about, SIGNAL(triggered()), this, SLOT(showAbout()));
	menu->addAction(about);

	// add quit action
	quit = new QAction(tr("Exit"), this);
	menu->addAction(quit);
	connect(quit, SIGNAL(triggered()), qApp, SLOT(quit()));

	// set proper icon based on state
	QString icon_name = (mountedNumber > 0) ? "mount" : "unmount";
	tray->setIcon(QIcon(":/icons/icons/cdrom_" + icon_name + ".png"));

	// set proper toolkit
	QString tooltip_text = (device_number > 0) ? QString(tr("%1 available device(s)")).arg(device_number) :
		tr("CDemu not running");
	tray->setToolTip("QCDemu: " + tooltip_text);
}

// change bus to be used
void QCDemu::selectBus(const bool busType)
{
	delete iface;

	dbusConnect(busType);
	useSystemBus = busType;
	updateActions();
}

// auto-detect if the other bus should be used
bool QCDemu::guessBus()
{
	delete iface;
	dbusConnect(!useSystemBus);

	return getDeviceNumber();
}


// say your name
void QCDemu::showAbout()
{
	setWindowIcon(QIcon(":/icons/icons/cdrom_mount.png"));
	QMessageBox::about(this, tr("About QCDemu"), tr("CDemu v" VERSION " by mziab"));
}

// purge recent file list
void QCDemu::recentClear()
{
	recentFiles.clear();
	updateActions();
}

// check if cdemu daemon is new enough :)
bool QCDemu::daemonIsNewEnough()
{
	QDBusMessage reply = iface->call("GetDaemonVersion");
	if (reply.type() == QDBusMessage::ErrorMessage)
		return false;
	else
	{
		QString minor_ver = reply.arguments().first().toString();
		minor_ver = minor_ver.mid(2, 1);

		return (minor_ver.toInt() >= 2);
	}
}

// return status of a given device
QVariantList QCDemu::getStatus(const int device)
{
	QDBusMessage reply = iface->call("DeviceGetStatus", device);
	return(reply.arguments());
}

// return number of available devices
unsigned int QCDemu::getDeviceNumber()
{
	QDBusMessage reply = iface->call( "GetNumberOfDevices");
	return (reply.type() != QDBusMessage::ErrorMessage) ? (reply.arguments().first().toInt()) : 0;
}

